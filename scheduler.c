#include "scheduler.h"

static int buscarIndex_empty(array_task *_array);
static void initArray(array_task *_array);
static bool isOk_ID_Task(array_task *_array, task _t);

task *crear_Task(int _id, int _delay, char *_nombreTask, char *_proceso_realizar, int _estado){
	task *_t = (task *)malloc(sizeof(task));
	_t->id = _id;
	_t->delay = _delay;
	strcpy(_t->nombreTask, _nombreTask);
	strcpy(_t->proceso_realizar, _proceso_realizar);
	_t->estado = _estado;
	return _t;
}

void agregar_Task(array_task *_array, task _t){
	int _index = buscarIndex_empty(_array);
	bool id_ok = isOk_ID_Task(_array, _t);
	if(_index != -1){
		if(id_ok != false){
			_array[_index].tarea = _t;
			_array[_index].empty = false;
			printf("Se agrego a [_array] la tarea [%s] con id [%d] <---- OK\n", _array[_index].tarea.nombreTask, _array[_index].tarea.id);
			cont_tasks++;
		}
		else{
			printf("El ID [%d] ya se encuentra asociado a una Task...\n", _t.id);
		}
	}
	else{
		printf("NO hay index disponible...\n");
	}
}

void ejecutar_Tasks(array_task *_array){
	printf("-----> Tasks = [%d]\n", cont_tasks);
	for(int i = 0; i < cont_tasks; i++){
		if(_array[i].tarea.estado == ACTIVO){
			printf("<=================================================================================>\n");
			printf("\tLa tarea de nombre [%s], con ID [%d], se encuentra realizando el proceso [%s] con un delay de [%d] segundo(s)\n", _array[i].tarea.nombreTask, _array[i].tarea.id, _array[i].tarea.proceso_realizar, _array[i].tarea.delay);
			printf("<=================================================================================>\n");
			for(int j = 0; j < _array[i].tarea.delay; j++){
				sleep(1);
			}
		}
		else if(_array[i].tarea.estado == NO_ACTIVO){
			printf("<+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++>\n");
			printf("\tLa tarea de nombre [%s], con ID [%d], SE ENCUENTRA EN ESTADO NO ACTIVO\n", _array[i].tarea.nombreTask, _array[i].tarea.id);
			printf("<+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++>\n");
		}
		else{
			;
		}
	}
}

array_task *crear_ArrayTasks(void){
	array_task *array = (array_task *)malloc(sizeof(array_task)*MAX_TASK);
	initArray(array);
	return array;
}

/* PRIVATE FUNCTIONS */

static bool isOk_ID_Task(array_task *_array, task _t){
	bool id_Ok = true;
	for(int i = 0; i < MAX_TASK; i++){
		if(_array[i].tarea.id == _t.id){
			id_Ok = false;
			break;
		}
	}
	return id_Ok;
}

static void initArray(array_task *_array){
	for(int i = 0; i < MAX_TASK; i++){
		_array[i].tarea.id = -1;
		_array[i].empty = true;
	}
}

static int buscarIndex_empty(array_task *_array){
	int _index_empty = -1;
	for(int i = 0; i < MAX_TASK; i++){
		if(_array[i].empty == true){
			_index_empty = i;
			break;
		}
	}
	return _index_empty;
}
